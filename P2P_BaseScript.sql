/*

------------------------------------------------------------------------------------------------------------------
-- SAP (R3+S4), Purchase to Pay (P2P) Base script
------------------------------------------------------------------------------------------------------------------



================================================================================================================
---: Drop Tables for reimport
--==============================================================================================================
Step 0 Reimport:

Drop table IF EXISTS BKPF;
Drop table IF EXISTS BSEG;
Drop table IF EXISTS CDHDR;
Drop table IF EXISTS CDPOS;
Drop table IF EXISTS DD07T;
Drop table IF EXISTS EBAN;
Drop table IF EXISTS EKBE;
Drop table IF EXISTS EKKO;
Drop table IF EXISTS EKPO;
Drop table IF EXISTS NAST;
Drop table IF EXISTS RSEG;
Drop table IF EXISTS T008T;
Drop table IF EXISTS USR02;



*/
--================================================================================================================
--0: Create eventlog table
--==============================================================================================================

Drop table IF EXISTS eventlog;
CREATE TABLE eventlog
(
    "CaseID" NVARCHAR(50),
    "MANDT" NCHAR(3),
    "EBELN" NVARCHAR(10),
    "EBELP" NVARCHAR(5),
    "ACTIVITY_EN" NVARCHAR(200),
    "ACTIVITY_DETAIL_EN" NVARCHAR(300),
    "EVENTTIME" datetime,
    "USER_NAME" NVARCHAR(50),
    "USER_TYPE" NVARCHAR(10),
    "CHANGED_TABLE" NVARCHAR(20),
    "CHANGED_FIELD" NVARCHAR(20),
    "CHANGED_FROM" NVARCHAR (300),
    "CHANGED_TO" NVARCHAR(300),
    "CHANGED_FROM_FLOAT" FLOAT,
    "CHANGED_TO_FLOAT" FLOAT,
    "CHANGE_NUMBER" NVARCHAR(50),
    "TRANSACTION_CODE" NVARCHAR(20),
);


--================================================================================================================
--0: Create datamodel (Help tables)
--================================================================================================================


DROP TABLE IF EXISTS EKKO_EKPO;

--Create help table EKKO_EKPO + Insert into:

SELECT DISTINCT
    EKKO.MANDT,
    EKKO.EBELN,
    EKPO.EBELP,
    EKKO.AEDAT,
    EKKO.ERNAM,
    CONCAT(EKKO.MANDT,EKKO.EBELN,EKPO.EBELP) AS "CaseID",
    EKPO.BANFN,
    EKPO.BNFPO
INTO EKKO_EKPO
FROM dbo.EKKO AS EKKO
    INNER JOIN dbo.EKPO AS EKPO ON
EKKO.MANDT = EKPO.MANDT AND
        EKKO.EBELN = EKPO.EBELN
--WHERE
--    EKKO.BSTYP <> 'K'
;

--Create help table CDHDR_CDPOS + Insert into:

DROP TABLE IF EXISTS CDHDR_CDPOS;

SELECT DISTINCT
    CDHDR.MANDANT,
    CDHDR.CHANGENR,
    CDHDR.OBJECTCLAS,
    CDHDR.OBJECTID,
    CDHDr.UDATE,
    CDHDR.UTIME,
    CDHDR.TCODE,
    CDHDR.USERNAME,
    CDPOS.TABNAME,
    CDPOS.FNAME,
    CDPOS.VALUE_OLD,
    CDPOS.VALUE_NEW,
    CDPOS.TABKEY,
    CDPOS.CHNGIND
INTO CDHDR_CDPOS
FROM dbo.CDHDR AS CDHDR
    INNER JOIN dbo.CDPOS AS CDPOS ON
CDHDR.MANDANT = CDPOS.MANDANT AND
        CDHDR.CHANGENR = CDPOS.CHANGENR AND
        CDHDR.OBJECTCLAS = CDPOS.OBJECTCLAS and
        CDHDR.OBJECTID = CDPOS.OBJECTID


--================================================================================================================
--1: Insert data in eventlog
--================================================================================================================

-- Activity 01 = Create Purchase Requisition Item

--insert into eventlog
--select
--  e.caseid as "caseid",
--	e.mandt as "mandt",
--	e.ebeln as "ebeln",
--	e.ebelp as "ebelp",
--	'create purchase requisition item' as "activity_en",
--  '' as "activity_detail_en",
--	case
--		when coalesce(c.udate, '') <> '' then convert(varchar, convert(datetime, c.udate), 111)
--+ ' ' + substring(c.utime, 1, 2)
--+ ':' + substring(c.utime, 3, 2)
--+ ':' + substring(c.utime, 5, 2)
--		else eban.badat
--	end as "eventtime",
--  eban.ernam as "user_name",
--  usr02.ustyp as "user_type",
--  c.tabname as "changed_table",
--  c.fname as "changed_field",
--  c.value_old as "changed_from",
--  c.value_new as "changed_to",
--  '' as "changed_from_float",
--  '' as "changed_to_float",
--  '' as "change_number",
--  c."tcode" as "transaction_code"
--from ekko_ekpo as e
--inner join eban as eban on
--eban.mandt = e.mandt and
--eban.banfn = e.banfn and
--eban.bnfpo = e.bnfpo
--left join cdhdr_cdpos as c on
--eban.mandt = c.mandant and
--concat(eban.mandt,eban.banfn,eban.bnfpo) = c.tabkey and
--c.objectclas = 'banf' and
--c.chngind = 'i' and
--c.tabname = 'eban'
--left join usr02 as usr02 on
--eban.mandt = usr02.mandt and
--eban.ernam = usr02.bname
--where
--coalesce(eban.ebeln, '') <> '';
--============================================== Creation of purchase requsition was based on info in purchase order table in stead of purchase order requisition


INSERT INTO eventlog
SELECT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    'Create Purchase Requisition Item' AS "ACTIVITY_EN",
    eban.banfn AS "ACTIVITY_DETAIL_EN",
    eban.BADAt "EVENTTIME",
    EBAN.ERNAM AS "USER_NAME",
    USR02.USTYP AS "USER_TYPE",
    '' AS "CHANGED_TABLE",
    '' AS "CHANGED_FIELD",
    '' AS "CHANGED_FROM",
    '' AS "CHANGED_TO",
    '' AS "CHANGED_FROM_FLOAT",
    '' AS "CHANGED_TO_FLOAT",
    '' AS "CHANGE_NUMBER",
    '' AS "TRANSACTION_CODE"
FROM EBAN
    INNER JOIN EKKO_EKPO AS E ON
EBAN.MANDT = E.MANDT AND
        EBAN.EBELN = E.EBELN AND
        EBAN.EBELP = E.EBELP
    LEFT JOIN USR02 AS USR02 ON
EBAN.MANDT = USR02.MANDT AND
        EBAN.ERNAM = USR02.BNAME;

--Purchase order requsition do not fit with the EBAN table


-- Activity 02 = Release Purchase Requisition
-- Activity 03 = Refuse Purchase Requisition

INSERT INTO eventlog
SELECT DISTINCT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    CASE
		WHEN C.VALUE_NEW = '2' THEN 'Release Purchase Requisition'
		WHEN C.VALUE_NEW = 'X' THEN 'Refuse Purchase Requisition'
	END AS "ACTIVITY_EN",
    '' AS "ACTIVITY_DETAIL_EN",
    -- FIX: Convert UTIME for substring
    convert(varchar, convert(datetime, c.UDATE), 111)
+ ' ' + substring(CONVERT(varchar,c.UTIME), 1, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 4, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 7, 2) AS "EVENTTIME",
    EBAN.ERNAM AS "USER_NAME",
    USR02.USTYP AS "USER_TYPE",
    C.TABNAME AS "CHANGED_TABLE",
    C.FNAME AS "CHANGED_FIELD",
    C.VALUE_OLD AS "CHANGED_FROM",
    C.VALUE_NEW AS "CHANGED_TO",
    '' AS "CHANGED_FROM_FLOAT",
    '' AS "CHANGED_TO_FLOAT",
    '' AS "CHANGE_NUMBER",
    C.TCODE AS "TRANSACTION_CODE"
FROM
    CDHDR_CDPOS AS C
    INNER JOIN EBAN AS EBAN ON
C.MANDANT = EBAN.MANDT AND
        C.TABKEY = CONCAT(EBAN.MANDT,EBAN.BANFN,EBAN.BNFPO)
    INNER JOIN EKKO_EKPO AS E ON
E.MANDT = EBAN.MANDT AND
        E.EBELN = EBAN.EBELN AND
        E.EBELP = EBAN.EBELP
    LEFT JOIN USR02 AS USR02 ON
C.MANDANT = USR02.MANDT AND
        C.USERNAME = USR02.BNAME
WHERE
C.OBJECTCLAS = 'BANF' AND
    C.FNAME = 'FRGKZ' AND
    C.VALUE_NEW IN ('2', 'X');

-- Activity 04 = Create Purchase Order Item
-- Remarks:
--	- In addition to the creation of purchase requisition items above, the change tables are joined twice
--	- Reason: In the best case, the precise time stamp is given on an item level; otherwise, one can still look for precise creation dates of the header
INSERT INTO eventlog
SELECT DISTINCT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    'Create Purchase Order Item' AS "ACTIVITY_EN",
    '' as ACTIVITY_DETAIL_EN,
    CASE
		WHEN COALESCE(C_POS.UDATE, '') <> '' THEN
convert(varchar, convert(datetime, C_POS.UDATE), 111)
+ ' ' + substring(CONVERT(varchar,C_POS.UTIME), 1, 2)
+ ':' + substring(CONVERT(varchar,C_POS.UTIME), 4, 2)
+ ':' + substring(CONVERT(varchar,C_POS.UTIME), 7, 2)
		WHEN COALESCE(C_HEAD.UDATE, '') <> '' THEN
convert(varchar, convert(datetime, C_HEAD.UDATE), 111)
+ ' ' + substring(CONVERT(varchar,C_HEAD.UTIME), 1, 2)
+ ':' + substring(CONVERT(varchar,C_HEAD.UTIME), 4, 2)
+ ':' + substring(CONVERT(varchar,C_HEAD.UTIME), 7, 2)
ELSE convert(varchar, convert(datetime, E.AEDAT), 111)                          -- !! YYYY/MM/DD HH:MM:SS Format
	END AS "EVENTTIME",
    COALESCE(C_POS.USERNAME, C_POS.USERNAME, E.ERNAM)  As "USER_NAME",
    COALESCE(USR02_POS.USTYP, USR02_HEAD.USTYP) AS "USER_TYPE",
    COALESCE(C_POS.TABNAME, C_HEAD.TABNAME) AS "CHANGED_TABLE",
    COALESCE(C_POS.FNAME, C_HEAD.FNAME) AS "CHANGED_FIELD",
    COALESCE(C_POS.VALUE_OLD, C_HEAD.VALUE_OLD) AS "CHANGED_FROM",
    COALESCE(C_POS.VALUE_NEW, C_HEAD.VALUE_NEW) AS "CHANGED_TO",
    '' AS "CHANGED_FROM_FLOAT",
    '' AS "CHANGED_TO_FLOAT",
    '' AS "CHANGE_NUMBER",
    COALESCE(C_POS."TCODE", C_HEAD."TCODE") AS "TRANSACTION_CODE"
FROM
    EKKO_EKPO AS E
    LEFT JOIN CDHDR_CDPOS  AS C_POS ON
C_POS.TABKEY = concat(E.MANDT,E.EBELN,E.EBELP) and
        C_POS.TABNAME = 'EKPO' and
        C_POS.CHNGIND = 'I' and
        C_POS.FNAME = 'KEY'
    LEFT JOIN CDHDR_CDPOS AS C_HEAD ON
C_HEAD.TABKEY= concat(E.MANDT,E.EBELN) and
        C_HEAD.TABNAME = 'EKKO' and
        C_HEAD.CHNGIND = 'I' and
        C_HEAD.FNAME = 'KEY'
    LEFT JOIN USR02  AS USR02_POS ON
C_POS.MANDANT = USR02_POS.MANDT and
        C_POS.USERNAME = USR02_POS.BNAME
    LEFT JOIN USR02  AS USR02_HEAD ON
C_HEAD.MANDANT = USR02_HEAD.MANDT and
        C_HEAD.USERNAME = USR02_HEAD.BNAME;


-- STOP HERE! 20190422 18:07

-- Activity 05 = Block Purchase Order Item
-- Activity 06 = Reactivate Purchase Order Item
-- Activity 07 = Delete Purchase Order Item
-- Activity 08 = Change Deletion Flag for Purchase Order Item
-- Activity 09 = Change Price
-- Activity 10 = Change Quantity
-- Activity 11 = Change Storage Location
-- Activity 12 = Receive Order Confirmation
-- Activity 13 = Update Order Confirmation
-- Activity 14 = Change Delivery Indicator
-- Activity 15 = Change Final Invoice Indicator
-- Activity 16 = Change Outward Delivery Indicator
-- Activity 17 = Change Rejection Indicator

INSERT INTO eventlog
SELECT DISTINCT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    CASE
		WHEN C.FNAME = 'LOEKZ' AND C.VALUE_NEW ='S' THEN 'Block Purchase Order Item'
		WHEN C.FNAME = 'LOEKZ' AND C.VALUE_OLD ='S' AND COALESCE(C.VALUE_NEW, '') = ''  THEN 'Reactivate Purchase Order Item'
		WHEN C.FNAME = 'LOEKZ' AND C.VALUE_NEW ='L' THEN 'Delete Purchase Order Item'
		WHEN C.FNAME = 'LOEKZ' AND C.VALUE_OLD ='L' AND COALESCE(C.VALUE_NEW, '') = '' THEN 'Reactivate Purchase Order Item'
		WHEN C.FNAME = 'LOEKZ' THEN 'Change Deletion Flag for Purchase Order Item'
		WHEN C.FNAME = 'NETPR' THEN 'Change Price'
		WHEN C.FNAME = 'MENGE' THEN 'Change Quantity'
		WHEN C.FNAME = 'LGORT' THEN 'Change Storage Location'
		WHEN C.FNAME = 'LABNR' AND COALESCE(C.VALUE_OLD, '') = '' THEN 'Receive Order Confirmation'
		WHEN C.FNAME = 'LABNR' THEN 'Update Order Confirmation'
		WHEN C.FNAME = 'ELIKZ' THEN 'Change Delivery Indicator'
		WHEN C.FNAME = 'EREKZ' THEN 'Change Final Invoice Indicator'
		WHEN C.FNAME = 'EGLKZ' THEN 'Change Outward Delivery Indicator'
		WHEN C.FNAME = 'ABSKZ' THEN 'Change Rejection Indicator'
		
        -- Johann's edit linked to EKPO changes
		-- WHEN C.FNAME = 'KNTTP' THEN 'Chane Account Assignment Category'
		-- WHEN C.FNAME = 'PEINH' THEN 'Change Price Unit'
		-- WHEN C.FNAME = 'WEBRE' THEN 'Change GR Based Invoice Verification'
		-- WHEN C.FNAME = 'MWSKZ' THEN 'Change Sales Tax Code'
        -- WHEN C.FNAME = 'KONNR' THEN 'Change No of Purchasing Agreement'
		-- WHEN C.FNAME = 'UEBTO' THEN 'Change Overdelivery Allowed'
		-- WHEN C.FNAME = 'UEBTK' THEN 'Change Overdelivery Tolerance Limit'
        -- WHEN C.FNAME = 'WEPOS' THEN 'Change GR Indicator'
        -- WHEN C.FNAME = 'BANFN' THEN 'Change PR Number'
		-- WHEN C.FNAME = 'MATNR' THEN 'Change Material Number'
		-- WHEN C.FNAME = 'UNTTO' THEN 'Change Underdelivery Tolerance'
        -- WHEN C.FNAME = 'REPOS' THEN 'Change Invoice Receipt Indicator'
		-- WHEN C.FNAME = 'PSTYP' THEN 'Change Item Category'
		-- WHEN C.FNAME = 'MATKL' THEN 'Change Material Group'
		-- WHEN C.FNAME = 'BPRME' THEN 'Change Order Price Unit'
        -- WHEN C.FNAME = 'TXZ01' THEN 'Change Short Text'
		-- WHEN C.FNAME = 'IDNLF' THEN 'Change Material Number Used By Vendor'
        -- WHEN C.FNAME = 'RETPO' THEN 'Change Returns Item'
		-- WHEN C.FNAME = 'KTPNR' THEN 'Change Item Number of Principal Purchase Agreement'
		-- WHEN C.FNAME = 'SPINF' THEN 'Change Indicator: Update Info Record'
		-- WHEN C.FNAME = 'LMEIN' THEN 'Change Base Unit of Measure'
		-- WHEN C.FNAME = 'BEDNR' THEN 'Change Requirement Tracking Number'
		
        -- Changes linked to EKKO
        -- WHEN C.FNAME = 'EKGRP' THEN 'Change Purchasing Group'
		-- WHEN C.FNAME = 'BSART' THEN 'Change Purchasing Document Type'
		-- WHEN C.FNAME = 'INCO1' THEN 'Change Incoterms'
        -- WHEN C.FNAME = 'LFRNR' THEN 'Change Vendor Account Number'
		-- WHEN C.FNAME = 'BUKRS' THEN 'Change Company Code'
		-- WHEN C.FNAME = 'EKORG' THEN 'Change Purchasing organization'
		-- WHEN C.FNAME = 'ZTERM' THEN 'Change Term of Payment Key'
		-- WHEN C.FNAME = 'WAERS' THEN 'Change Currency Key'
	
        -- Changes linked to EKKET NOT PART OF OBJECT C --54 occurances
        -- WHEN C.FNAME = 'BANFN' THEN 'Change Purchasing Requisition Number'
    
        -- Changes linked to EKES NOT PART OF OBJECT C - 1 occurance
        -- WHEN C.FNAME = 'MENGE' THEN 'Change Vendor Confirmation QTY'
        -- Query CDPOS - # of changes with EKES/ EKET in tabname 

    	-- Johann's edit
	END AS "ACTIVITY_EN",
    '' AS "ACTIVITY_DETAIL_EN",
    convert(varchar, convert(datetime, c.UDATE), 111)
+ ' ' + substring(CONVERT(varchar,c.UTIME), 1, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 4, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 7, 2) AS "EVENTTIME",
    C.USERNAME AS "USER_NAME",
    USR02.USTYP AS "USER_TYPE",
    C.TABNAME AS "CHANGED_TABLE",
    C.FNAME AS "CHANGED_FIELD",
    C.VALUE_OLD AS "CHANGED_FROM",
    C.VALUE_NEW AS "CHANGED_TO",
    CASE
	    WHEN C.FNAME = 'NETPR' THEN cast(C.VALUE_OLD AS float)
	    ELSE cast(NULL AS float)
	END AS "CHANGED_FROM_FLOAT",
    CASE
	    WHEN C.FNAME = 'NETPR' THEN cast(C.VALUE_NEW AS float)
	    ELSE cast(NULL AS float)
	END AS "CHANGED_TO_FLOAT",
    C.CHANGENR AS "CHANGE_NUMBER",
    C.TCODE AS "TRANSACTION_CODE"
FROM CDHDR_CDPOS AS C
    INNER JOIN EKKO_EKPO AS E ON
C.OBJECTID = E.EBELN AND
        C.TABKEY = CONCAT(E.MANDT,E.EBELN,E.EBELP) AND
        C.MANDANT = E.MANDT
    LEFT JOIN USR02 AS USR02 ON
USR02.MANDT = C.MANDANT AND
        USR02.BNAME = C.USERNAME
WHERE
C.FNAME IN ('LOEKZ', 'NETPR',  'MENGE', 'LABNR','LGORT','ELIKZ', 'EREKZ','EGLKZ','ABSKZ');

-- Johann EDit
-- C.FNAME IN ('LOEKZ', 'NETPR',  'MENGE', 'LABNR','LGORT','ELIKZ', 'EREKZ','EGLKZ','ABSKZ','KNTTP','PEINH','WEBRE','EKGRP','MWSKZ','EKGRP','BSART','KONNR','UEBTO','UEBTK','BANFN','MATNR','UNTTO','REPOS','PSTYP','MATKL','INCO1','BPRME','LFRNR','BUKRS','EKORG','TXZ01','IDNLF' ,'RETPO' ,'KTPNR' ,'SPINF' , 'LMEIN','BEDNR' ,'ZTERM' ,'WAERS');
-- Johann Edit end


-- Activity 18 = Change Vendor
-- Activity 19 = Release Purchase Order
-- Activity 20 = Refuse Purchase Order
-- Activity 21 = Change Approval for Purchase Order
-- Activity 22 = Change Currency

INSERT INTO eventlog
SELECT DISTINCT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    CASE
	    WHEN C.FNAME = 'LIFNR' THEN 'Change Vendor'
		WHEN C.FNAME = 'FRGKE' AND C.VALUE_NEW = '1' THEN 'Release Purchase Order'
		WHEN C.FNAME = 'FRGKE' AND C.VALUE_NEW = '2' THEN 'Refuse Purchase Order'
		WHEN C.FNAME = 'FRGKE' THEN 'Change Approval for Purchase Order'
		WHEN C.FNAME = 'WAERS' THEN 'Change Currency'
	END AS "ACTIVITY_EN",
    CONCAT('FRGKE set from ',C.VALUE_OLD,' to ',C.VALUE_NEW) AS "ACTIVITY_DETAIL_EN",
    convert(varchar, convert(datetime, c.UDATE), 111)
+ ' ' + substring(CONVERT(varchar,c.UTIME), 1, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 4, 2)
+ ':' + substring(CONVERT(varchar,c.UTIME), 7, 2) AS "EVENTTIME",
    C.USERNAME As "USER_NAME",
    USR02.USTYP AS "USER_TYPE",
    C.TABNAME AS "CHANGED_TABLE",
    C.FNAME AS "CHANGED_FIELD",
    C.VALUE_OLD AS "CHANGED_FROM",
    C.VALUE_NEW AS "CHANGED_TO",
    '' AS "CHANGED_FROM_FLOAT",
    '' AS "CHANGED_TO_FLOAT",
    C.CHANGENR AS "CHANGE_NUMBER",
    C.TCODE AS "TRANSACTION_CODE"
FROM
    CDHDR_CDPOS AS C
    INNER JOIN EKKO_EKPO AS E ON
C.TABKEY = CONCAT(E.MANDT,E.EBELN) AND
        C.OBJECTID = E.EBELN AND
        C.MANDANT = E.MANDT
    LEFT JOIN USR02 AS USR02 ON
USR02.MANDT = C.MANDANT AND
        USR02.BNAME = C.USERNAME
WHERE
C.FNAME IN ('LIFNR', 'WAERS', 'FRGKE') AND
    C.TABNAME = 'EKKO';


-- Activity 23 = Send Purchase Order Update
-- Activity 24 = Send Purchase Order
-- Activity 25 = Send Overdue Notice
-- Activity 26 = Dun Order Confirmation

INSERT INTO eventlog
SELECT DISTINCT
    E.CaseID AS "CaseID",
    E.MANDT AS "MANDT",
    E.EBELN AS "EBELN",
    E.EBELP AS "EBELP",
    CASE WHEN NAST.KAPPL ='EF' AND NAST.KSCHL IN  ('NEU') AND NAST.AENDE = 'X' THEN 'Send Purchase Order Update'
		WHEN NAST.KAPPL ='EF' AND NAST.KSCHL IN  ('NEU')  THEN 'Send Purchase Order'
		WHEN NAST.KAPPL LIKE 'EF' AND NAST.KSCHL = 'MAHN' THEN 'Send Overdue Notice'
		WHEN NAST.KAPPL = 'EF' AND NAST.KSCHL = 'AUFB' THEN 'Dun Order Confirmation'
	END AS "ACTIVITY_EN",
    CONCAT('PO sent by ',DD07T.DDTEXT + '; Title: ', ISNULL(NAST.TDCOVTITLE,0))  AS "ACTIVITY_DETAIL_EN",
    convert(varchar, convert(datetime, NAST.DATVR), 111)
+ ' ' + substring(CONVERT(varchar,NAST.UHRVR), 1, 2)
+ ':' + substring(CONVERT(varchar,NAST.UHRVR), 4, 2)
+ ':' + substring(CONVERT(varchar,NAST.UHRVR), 7, 2)  AS "EVENTTIME",
    nast.USNAM as "USERNAME", --NOTSURE
    USR02.USTYP AS "USER_TYPE",
    '' AS "CHANGED_TABLE",
    '' AS "CHANGED_FIELD",
    '' AS "CHANGED_FROM",
    '' AS "CHANGED_TO",
    '' AS "CHANGED_FROM_FLOAT",
    '' AS "CHANGED_TO_FLOAT",
    '' AS "CHANGE_NUMBER",
    NAST.TCODE AS "TRANSACTION_CODE"
FROM NAST AS NAST
    INNER JOIN EKKO_EKPO AS E ON
NAST.MANDT = E.MANDT AND
        NAST.OBJKY = E.EBELN
    LEFT JOIN DD07T AS DD07T ON
DD07T.DOMVALUE_L = NAST.NACHA AND
        DD07T.DOMNAME = 'NA_NACHA' AND
        DD07T.DDLANGUAGE = 'D' collate sql_latin1_general_cp1_cs_as
    LEFT JOIN USR02 AS USR02 ON
NAST.MANDT = USR02.MANDT AND
        NAST.USNAM = USR02.BNAME
WHERE
COALESCE(NAST.DATVR, '') <> '' AND
    NAST.KAPPL = 'EF' AND
    NAST.KSCHL IN ('NEU','MAHN','AUFB');

-- Activity 27 = Record Goods Receipt
-- Activity 28 = Cancel Goods Receipt'
-- Activity 29 = Record Goods Receipt'
-- Activity 30 = Record Service Entry Sheet'
-- Activity 31 = Cancel Service Entry Sheet'
-- Activity 32 = Record Service Entry Sheet'
-- Activity 33 = Record Invoice Receipt'
-- Activity 34 = Cancel Invoice Receipt'
-- Activity 35 = Record Invoice Receipt'


-- We don't have those tables.... Comment out!

-- INSERT INTO eventlog
-- SELECT DISTINCT
--     E.CaseID AS "CaseID",
--     E.MANDT AS "MANDT",
--     E.EBELN AS "EBELN",
--     E.EBELP AS "EBELP",
--     CASE
-- 		WHEN EKBE.VGABE = '1' AND EKBE.SHKZG = 'S' THEN 'Record Goods Receipt'
-- 		WHEN EKBE.VGABE = '1' AND EKBE.SHKZG = 'H' THEN 'Cancel Goods Receipt'
-- 		WHEN EKBE.VGABE = '1' THEN  'Record Goods Receipt'
-- 		WHEN EKBE.VGABE = '9' AND EKBE.SHKZG = 'S' THEN 'Record Service Entry Sheet'
-- 		WHEN EKBE.VGABE = '9' AND EKBE.SHKZG = 'H' THEN 'Cancel Service Entry Sheet'
-- 		WHEN EKBE.VGABE = '9' THEN 'Record Service Entry Sheet'
-- 		WHEN EKBE.VGABE = '2' AND EKBE.SHKZG = 'S' THEN 'Record Mat Invoice Receipt'
-- 		WHEN EKBE.VGABE = '2' AND EKBE.SHKZG = 'H' THEN 'Cancel Invoice Receipt'
-- 		WHEN EKBE.VGABE = '2' THEN  'Record Mat Invoice Receipt'
-- 	END AS "ACTIVITY_EN",
--     '' AS "ACTIVITY_DETAIL_EN",
--     convert(varchar, convert(datetime, EKBE.CPUDT), 111)
-- + ' ' + substring(EKBE.CPUTM, 1, 2)
-- + ':' + substring(EKBE.CPUTM, 3, 2)
-- + ':' + substring(EKBE.CPUTM, 5, 2) AS "EVENTTIME",
--     EKBE.ERNAM AS "USER_NAME",
--     USR02.USTYP AS "USER_TYPE",
--     '' AS "CHANGED_TABLE",
--     '' AS "CHANGED_FIELD",
--     '' AS "CHANGED_FROM",
--     '' AS "CHANGED_TO",
--     '' AS "CHANGED_FROM_FLOAT",
--     '' AS "CHANGED_TO_FLOAT",
--     '' AS "CHANGE_NUMBER",
--     '' AS "TRANSACTION_CODE"
-- FROM EKKO_EKPO AS E
--     INNER JOIN EKBE AS EKBE ON
-- E.MANDT = EKBE.MANDT AND
--         E.EBELN = EKBE.EBELN AND
--         E.EBELP = EKBE.EBELP
--     LEFT JOIN USR02 AS USR02 ON
-- USR02.MANDT = EKBE.MANDT AND
--         USR02.BNAME = EKBE.ERNAM
-- WHERE
-- EKBE.VGABE IN ('1','9','2');

-- -- Activity 36 = Remove Payment Block
-- -- Activity 37 = Set Payment Block


-- INSERT INTO eventlog
-- SELECT DISTINCT
--     concat(RSEG.MANDT,RSEG.EBELN,RSEG.EBELP) AS "CaseID",
--     RSEG.MANDT AS "MANDT",
--     RSEG.EBELN AS "EBELN",
--     RSEG.EBELP AS "EBELP",
--     CASE
-- 		WHEN COALESCE(C.VALUE_NEW, '') = '' THEN 'Remove Payment Block'
-- 		WHEN COALESCE(C.VALUE_OLD, '') = '' THEN 'Set Payment Block'
-- 	END AS "ACTIVITY_EN",
--     CASE
-- 		WHEN COALESCE(C.VALUE_NEW, '') = '' THEN Concat('Removed from "',OLD.TEXTL ,' (' + C.VALUE_OLD + ')" ','for ' , BKPF.MANDT,BKPF.BUKRS,BKPF.BELNR,BKPF.GJAHR)
-- 		WHEN COALESCE(C.VALUE_OLD, '') = '' THEN concat('Set to "' , NEW.TEXTL , ' (' , C.VALUE_NEW , ')" ' , 'for ' , BKPF.MANDT,BKPF.BUKRS,BKPF.BELNR,BKPF.GJAHR)
-- 	END AS "ACTIVITY_DETAIL_EN",
--     convert(varchar, convert(datetime, C.UDATE), 111)
-- + ' ' + substring(C.UTIME, 1, 2)
-- + ':' + substring(C.UTIME, 3, 2)
-- + ':' + substring(C.UTIME, 5, 2) AS "EVENTTIME",
--     C.USERNAME AS "USER_NAME",
--     USR02.USTYP AS "USER_TYPE",
--     C.TABNAME AS "CHANGED_TABLE",
--     C.FNAME AS "CHANGED_FIELD",
--     C.VALUE_OLD AS "CHANGED_FROM",
--     C.VALUE_NEW AS "CHANGED_TO",
--     '' AS "CHANGED_FROM_FLOAT",
--     '' AS "CHANGED_TO_FLOAT",
--     '' AS "CHANGE_NUMBER",
--     C.TCODE AS "TRANSACTION_CODE"
-- FROM RSEG AS RSEG
--     INNER JOIN EKKO_EKPO AS E ON
-- RSEG.MANDT = E.MANDT and
--         RSEG.EBELN = E.EBELN AND
--         RSEG.EBELP = E.EBELP
--     INNER JOIN BKPF AS BKPF ON
-- BKPF.MANDT = RSEG.MANDT AND
--         SUBSTRING(BKPF.AWKEY,1,14) = CONCAT(RSEG.BELNR,RSEG.GJAHR)
--     INNER JOIN CDHDR_CDPOS AS C ON
-- C.MANDANT = BKPF.MANDT AND
--         C.OBJECTCLAS = 'BELEG' AND
--         C.OBJECTID = CONCAT(BKPF.MANDT,BKPF.BUKRS,BKPF.BELNR,BKPF.GJAHR) AND
--         C.FNAME = 'ZLSPR'
--     LEFT JOIN T008T AS OLD ON
-- OLD.MANDT = C.MANDANT AND
--         OLD.SPRAS = 'D' AND
--         C.VALUE_OLD = OLD.ZAHLS
--     LEFT JOIN T008T AS NEW ON
-- NEW.MANDT = C.MANDANT AND
--         NEW.SPRAS = 'D' AND
--         C.VALUE_NEW = NEW.ZAHLS
--     LEFT JOIN USR02 AS USR02 ON
-- C.MANDANT = USR02.MANDT AND
--         C.USERNAME = USR02.BNAME
-- WHERE
-- (COALESCE(C.VALUE_OLD, '') = '' OR COALESCE(C.VALUE_NEW, '') = '');

-- -- Activity 38 = Pay Invoice

-- INSERT INTO eventlog
-- SELECT DISTINCT
--     CONCAT(RSEG.MANDT,RSEG.EBELN,RSEG.EBELP) AS "CaseID",
--     RSEG.MANDT AS "MANDT",
--     RSEG.EBELN AS "EBELN",
--     RSEG.EBELP AS "EBELP",
--     'Pay Invoice' AS "ACTIVITY_EN",
--     '' AS "ACTIVITY_DETAIL_EN",
--     convert(varchar, convert(datetime, BKPF_Z.CPUDT), 111)
-- + ' ' + substring(BKPF_Z.CPUTM, 1, 2)
-- + ':' + substring(BKPF_Z.CPUTM, 3, 2)
-- + ':' + substring(BKPF_Z.CPUTM, 5, 2) AS "EVENTTIME",
--     BKPF_Z.USNAM AS "USER_NAME",
--     USR02.USTYP AS "USER_TYPE",
--     '' AS "CHANGED_TABLE",
--     '' AS "CHANGED_FIELD",
--     '' AS "CHANGED_FROM",
--     '' AS "CHANGED_TO",
--     '' AS "CHANGED_FROM_FLOAT",
--     '' AS "CHANGED_TO_FLOAT",
--     '' AS "CHANGE_NUMBER",
--     BKPF_Z.TCODE AS "TRANSACTION_CODE"
-- FROM RSEG AS RSEG
--     INNER JOIN EKKO_EKPO AS E ON
-- RSEG.MANDT = E.MANDT and
--         RSEG.EBELN = E.EBELN AND
--         RSEG.EBELP = E.EBELP
--     INNER JOIN BKPF AS BKPF ON
-- BKPF.MANDT = RSEG.MANDT AND
--         SUBSTRING(BKPF.AWKEY,1,14) = RSEG.BELNR + RSEG.GJAHR
--     INNER JOIN BSEG AS BSEG ON
-- BSEG.MANDT = BKPF.MANDT AND
--         BSEG.BUKRS = BKPF.BUKRS AND
--         BSEG.BELNR = BKPF.BELNR AND
--         BSEG.GJAHR = BKPF.GJAHR
--     LEFT JOIN BKPF AS BKPF_Z ON
-- BSEG.MANDT = BKPF_Z.MANDT AND
--         BSEG.BUKRS = BKPF_Z.BUKRS AND
--         BSEG.AUGBL = BKPF_Z.BELNR AND
--         YEAR(BSEG.AUGDT) = BKPF_Z.GJAHR
--     LEFT JOIN USR02 AS USR02 ON
-- BKPF_Z.MANDT = USR02.MANDT AND
--         BKPF_Z.USNAM = USR02.BNAME
-- WHERE
-- BSEG.KOART = 'K' AND
--     COALESCE(BSEG.AUGBL, '') <> '' AND
--     COALESCE(BKPF_Z.CPUDT, '') <> ''
-- 	--AND BSEG.AUGDT >= ;
